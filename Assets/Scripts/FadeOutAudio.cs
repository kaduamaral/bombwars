﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FadeOutAudio : MonoBehaviour
{
    float startFadeAt;
    float defaultVolume = 0.3f;

    AudioSource audioSource;
    // Start is called before the first frame update
    void Start()
    {
        startFadeAt = Time.time + 2;
        audioSource = GetComponent<AudioSource>();

        audioSource.volume = defaultVolume;
    }

    // Update is called once per frame
    void Update()
    {
        if (startFadeAt > Time.time) return;

        audioSource.volume -= 0.005f;
    }
}
