﻿using Photon.Pun;
using System;
using UnityEngine;
using UnityEngine.Tilemaps;

public enum Item
{
    Bomba,
    Caveira,
    Fogo,
    Luva,
    Patins,
    Pezinho,
    Soquinho
}

public enum Curse
{
    None,
    HighSpeed,
    InverseMovement,
    OneBomb,
    WeakFire,
    SlowExplosion,
    LowSpeed,
    StrongFire,
    FastExplosion,
    PutAllBombs,
    DropItems
}

public class ItemScript : MonoBehaviour
{

    public Item type;

    PhotonView photonView;

    BoxCollider2D boxCollider;

    Material material;

    Animator animator;

    Tilemap groundMap;

    SpriteRenderer render;

    float fade = 1f;

    bool isDissolving = false;
    public bool isFlying = false;
    Vector2 originalThrownPosition = Vector2.zero;
    Vector2 flyDirection = Vector2.zero;
    Vector2 landingPoint = Vector2.zero;
    float flySpeed = 0.15f;

    bool teleport = false;
    Vector2 teleportArea;

    Curse currentCurse = Curse.None;

    void Awake()
    {
        photonView = GetComponent<PhotonView>(); ;
        boxCollider = GetComponent<BoxCollider2D>();
        material = GetComponent<SpriteRenderer>().material;
        animator = GetComponent<Animator>();
        render = GetComponent<SpriteRenderer>();
        groundMap = GameObject.Find("Ground").GetComponent<Tilemap>();

        if (type == Item.Caveira && currentCurse == Curse.None)
        {
            SetCurse(Curse.HighSpeed);
        }
    }

    private void Update()
    {
        if (isDissolving)
        {
            fade -= Time.deltaTime * 1.2f;

            if (fade <= 0)
            {
                Debug.Log("Destroy Item by dissolve");
                DestroyItem();
                return;
            }

            material.SetFloat("_Fade", fade);
        }

        
    }

    void FixedUpdate()
    {
        if (!photonView.IsMine) return;

        if (isFlying)
        {

            Vector2 movement = transform.position;

            if (teleport)
            {
                movement = teleportArea;
                teleport = false;
            } else
            {
                float distance = Vector2.Distance(transform.position, landingPoint);

                if (distance <= 0.01)
                {
                    movement = Ground();
                }
                else
                {

                    if (flySpeed > distance)
                    {
                        movement = (Vector2)transform.position + (distance * flyDirection);
                    }
                    else
                    {
                        movement = (Vector2)transform.position + (flySpeed * flyDirection);
                    }
                }
            }

            

            transform.position = movement;
        }
    }

    void OnTriggerEnter2D(Collider2D collision)
    {
        if (!photonView.IsMine) return;

        if (!isFlying && collision.gameObject.CompareTag("Bomb") && !collision.gameObject.GetComponent<BombScript>().itsFlying)
        {
            Debug.Log("Destroy Item by bomb collision");
            DestroyItem();
        }
    }

    Vector2 Ground()
    {

        Collider2D[] colliders = Physics2D.OverlapBoxAll(landingPoint, new Vector2(0.25f, 0.25f), 0);
        foreach (Collider2D collider in colliders)
        {
            if (collider.gameObject.GetInstanceID() == gameObject.GetInstanceID()) continue;

            if (
                collider.gameObject.CompareTag("Brick") ||
                collider.gameObject.CompareTag("Bomb") ||
                collider.gameObject.CompareTag("Item") ||
                collider.gameObject.CompareTag("Wall")
            )
            {
                landingPoint = landingPoint + flyDirection;
                return Vector2.Lerp(transform.position, landingPoint, flySpeed);
            }
        }


        Vector2 nextPosition = landingPoint;
        isFlying = false;
        landingPoint = Vector2.zero;
        animator.SetBool("isFlippingVertical", false);
        animator.SetBool("isFlippingHorizontal", false);

        render.sortingLayerName = "Items";

        photonView.RPC("RPCSetFly", RpcTarget.Others, false);

        return nextPosition;
    }


    public void Burn()
    {
        if (isFlying) return;


        if (type == Item.Caveira)
        {
            // @TODO fix direction
            Vector2 direction = new Vector2(0, 1);
            Fly(direction);
        } else
        {
            boxCollider.enabled = false;
            isDissolving = true;
        }
        
    }

    public void Fly(Vector2 direction)
    {

        if (!photonView.IsMine)
        {
            photonView.RPC("RPCFly", photonView.Owner, direction.x, direction.y);
            return;
        }

        photonView.RPC("RPCSetFly", RpcTarget.Others, true);

        isFlying = true;
        flyDirection = direction;
        landingPoint = (Vector2)transform.position + (5 * flyDirection);
        originalThrownPosition = transform.position;
        render.sortingLayerName = "Air";




        if (flyDirection.x == 0)
        {
            animator.SetBool("isFlippingVertical", true);
        }
        else
        {
            animator.SetBool("isFlippingHorizontal", true);
        }

        
    }


    [PunRPC]
    void RPCFly(float x, float y)
    {
        Fly(new Vector2(x, y));
    }

    [PunRPC]
    void RPCSetFly(bool flying)
    {
        isFlying = flying;

        render.sortingLayerName = isFlying ? "Air" : "Items";
    }
    public void DestroyItem()
    {
        if (photonView.IsMine)
        {
            PhotonNetwork.Destroy(gameObject);
        } else
        {
            GetComponent<PhotonView>().RPC("RPCDestroyItem", photonView.Owner);
        }
    }

    [PunRPC]
    void RPCDestroyItem()
    {
        DestroyItem();
    }

    public void TeleportTo(Vector2 _teleportArea)
    {
        if (teleport)
        {
            return;
        }

        teleport = true;
        teleportArea = transform.position;

        if (_teleportArea.x != 0)
        {
            teleportArea.x = _teleportArea.x;

            float travelled = transform.position.x - originalThrownPosition.x;

            landingPoint.x = _teleportArea.x + (landingPoint.x - (originalThrownPosition.x + travelled));
        }
        else if (_teleportArea.y != 0)
        {
            teleportArea.y = _teleportArea.y;

            float travelled = transform.position.y - originalThrownPosition.y;

            // landingPoint.y = landingPoint.y - originalThrownPosition.y + _teleportArea.y - 1;
            landingPoint.y = _teleportArea.y + (landingPoint.y - (originalThrownPosition.y + travelled));
        }

        landingPoint = GetCellCenter(landingPoint);
    }

    public Vector2 GetCellCenter(Vector2 position)
    {
        Vector3Int cellPosition = groundMap.WorldToCell(position);
        return groundMap.GetCellCenterWorld(cellPosition);
    }

    public void SetCurse(Curse curse)
    {
        currentCurse = curse;
        photonView.RPC("RPCSetCurse", RpcTarget.Others, (int)curse);
    }

    public Curse GetCurse()
    {
        return currentCurse;
    }

    [PunRPC]
    void RPCSetCurse(int curse)
    {
        currentCurse = (Curse)curse;
    }

}
