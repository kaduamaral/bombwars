﻿using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class StageMessagesManager : MonoBehaviour
{
    bool isCounting = false;
    int count = -2;
    float nextCountTime;

    AudioSource sound;

    public GameObject mainMessage;
    public GameObject pingObject;
    public AudioClip shortBeep;
    public AudioClip longBeep;

    float deactivateMainTextAt = 0;


    // Start is called before the first frame update
    void Start()
    {
        sound = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {

        if (isCounting)
        {
            if (count >= 0 && Time.time >= nextCountTime)
            {
                Count();
            }
            else if (count == -1 && Time.time >= nextCountTime)
            {
                isCounting = false;
                HideMessage();
                count--;
            }
        }


        if (deactivateMainTextAt > 0 && Time.time >= deactivateMainTextAt)
        {
            Debug.Log("Disabling Message Canvas");
            deactivateMainTextAt = 0;
            mainMessage.SetActive(false);
        }

        UpdatePing();
    }


    public float StartCount(int countDown, float delay = 1.6f)
    {
        mainMessage.SetActive(true);
        count = countDown;
        nextCountTime = Time.time + delay;
        mainMessage.GetComponent<TextMeshProUGUI>().text = "Ready?";
        mainMessage.GetComponent<Animator>().SetTrigger("ShowInWinner");
        isCounting = true;

        return Time.time + countDown + delay + 1.2f;
    }

    void Count()
    {
        AudioClip clip;
        if (count == 0)
        {
            mainMessage.GetComponent<TextMeshProUGUI>().text = "GO!";
            clip = longBeep;
        }
        else
        {
            mainMessage.GetComponent<TextMeshProUGUI>().text = count.ToString();
            clip = shortBeep;
        }
        sound.PlayOneShot(clip);

        nextCountTime = Time.time + 1;
        count--;
    }

    public void ShowMessage(string message)
    {
        mainMessage.GetComponent<TextMeshProUGUI>().text = message;
        mainMessage.SetActive(true);
        mainMessage.GetComponent<Animator>().SetTrigger("ShowInWinner");
        deactivateMainTextAt = 0;
    }

    public void HideMessage()
    {
        mainMessage.GetComponent<Animator>().SetTrigger("ShowOffWinner");
        deactivateMainTextAt = Time.time + 1;
    }

    void UpdatePing()
    {
        pingObject.GetComponent<Text>().text = PhotonNetwork.GetPing().ToString() + "ms";
    }
}
