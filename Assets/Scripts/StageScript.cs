﻿using Photon.Pun;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEngine;

public class StageScript : MonoBehaviour
{

    public GameObject playerPrefab;

    public int playersInGame = 0;

    Color[] playerColors = new Color[4];

    public IDictionary<string, int> itemsAvailable = new Dictionary<string, int>();

    PhotonView photonView;

    GameObject spawnPoints;

    GameObject myPlayer;

    StageMessagesManager stageMessages;

    bool gameOver = false;

    float exitStage;
    bool stageExited = false;

    bool gameStarted = false;
    float timeToStart = 0;
    bool gameReady = false;

    


    // Start is called before the first frame update
    void Start()
    {
        Cursor.visible = false;
        photonView = GetComponent<PhotonView>();
        spawnPoints = GameObject.Find("SpawnPoints");

        stageMessages = GameObject.Find("Canvas").GetComponent<StageMessagesManager>();

        playerColors[0] = new Color(1, 0, 0);
        playerColors[1] = new Color(1, 0, 1);
        playerColors[2] = new Color(1, 1, 0);
        playerColors[3] = new Color(0, 1, 1);

        itemsAvailable["Caveira"] = 1;
        itemsAvailable["Patins"] = 3;
        itemsAvailable["Fogo"] = 4;
        itemsAvailable["Bomba"] = 6;
        itemsAvailable["Pezinho"] = 3;
        itemsAvailable["Luva"] = 2;
        itemsAvailable["Soquinho"] = 2;
        PutBricks();
        CreatePlayers();
    }

    private void Update()
    {
        if (!gameStarted && gameReady && timeToStart <= Time.time)
        {
            gameStarted = true;
        }

        if (gameStarted && !gameOver && playersInGame <= 1)
        {
            gameOver = true;
            exitStage = Time.time + 5;

            if (myPlayer && !myPlayer.GetComponent<PlayerController>().isDead())
            {
                myPlayer.GetComponent<Animator>().SetTrigger("Sorriu");
            }

            ShowEndMessage();
        }

        if (PhotonNetwork.IsMasterClient && gameOver && !stageExited && exitStage <= Time.time)
        {
            stageExited = true;
            PhotonNetwork.LoadLevel(0);
        }
    }

    public bool isGameOver()
    {
        return gameOver;
    }

    public bool isGameStarted()
    {
        return gameStarted;
    }

    private void CreatePlayers()
    {
        Transform spawnPoint = spawnPoints.transform.GetChild(PhotonNetwork.LocalPlayer.ActorNumber - 1);
        myPlayer = PhotonNetwork.Instantiate(Path.Combine("Prefabs", "PlayerNinja"), spawnPoint.position - (Vector3)PlayerController.FOOT_OFFSET, Quaternion.identity);

        photonView.RPC("RPCPlayerCreated", RpcTarget.All);
    }

    public void CreatedItem(string item)
    {
        itemsAvailable[item]--;

        if (PhotonNetwork.PlayerList.Length > 1)
        {
            photonView.RPC("UpdateItemsAvailable", RpcTarget.Others,
                itemsAvailable["Patins"],
                itemsAvailable["Fogo"],
                itemsAvailable["Bomba"],
                itemsAvailable["Pezinho"],
                itemsAvailable["Luva"],
                itemsAvailable["Caveira"]
            );
        }
    }

    [PunRPC]
    void RPCPlayerCreated()
    {
        playersInGame++;

        if (playersInGame == PhotonNetwork.PlayerList.Length)
        {
            StartGame();
        }
    }

    void StartGame()
    {
        gameReady = true;
        timeToStart = stageMessages.StartCount(3);
    }

    public void PlayerDie()
    {
        photonView.RPC("RPCPlayerDie", RpcTarget.All);
    }

    [PunRPC]
    void RPCPlayerDie()
    {
        playersInGame--;
    }

    [PunRPC]
    void UpdateItemsAvailable(int patins, int fogo, int bomba, int pezinho, int luva, int caveira)
    {
        itemsAvailable["Patins"] = patins;
        itemsAvailable["Fogo"] = fogo;
        itemsAvailable["Bomba"] = bomba;
        itemsAvailable["Pezinho"] = pezinho;
        itemsAvailable["Luva"] = luva;
        itemsAvailable["Caveira"] = caveira; 
    }

    void PutBricks()
    {
        if (!PhotonNetwork.IsMasterClient) return;

        

        for (float x = -5.5f; x < 7; x++)
        {
            for (float y = 3.5f; y > -7; y--)
            {
                if (isEmptySpot(x, y)) continue;
                else PutBrick(x, y);
            }
        }
    }

    void PutBrick(float x, float y)
    {
        PhotonNetwork.Instantiate(Path.Combine("Prefabs", "Brick"), new Vector3(x, y, 0), Quaternion.identity);
    }

    bool isEmptySpot(float x, float y)
    {
        int ix = Convert.ToInt32(Math.Truncate(x));
        int iy = Convert.ToInt32(Math.Truncate(y));

        if (
            (
                (x < 0 && (ix % 2) == 0) &&
                (y > 0 && (iy % 2) == 0)
            ) || (
                (x > 0 && (ix % 2) != 0) &&
                (y > 0 && (iy % 2) == 0)
            ) || (
                (x < 0 && (ix % 2) == 0) &&
                (y < 0 && (iy % 2) != 0)
            ) || (
                (x > 0 && (ix % 2) != 0) &&
                (y < 0 && (iy % 2) != 0)
            )
        )
        {
            return true;
        }


        Vector2[] emptyPoints = new Vector2[]
        {
            // Spot 1
            new Vector2(-5.5f, 3.5f),
            new Vector2(-4.5f, 3.5f),
            new Vector2(-5.5f, 2.5f),

            // Spot 2
            new Vector2(6.5f, -6.5f),
            new Vector2(5.5f, -6.5f),
            new Vector2(6.5f, -5.5f),

            // Spot 3
            new Vector2(-5.5f, -6.5f),
            new Vector2(-5.5f, -5.5f),
            new Vector2(-4.5f, -6.5f),

            // Spot 4
            new Vector2(6.5f, 3.5f),
            new Vector2(5.5f, 3.5f),
            new Vector2(6.5f, 2.5f),

            // Spot 5
            new Vector2(0.5f, -1.5f),
            new Vector2(0.5f, -0.5f),
            new Vector2(0.5f, -2.5f),

            new Vector2(-0.5f, -0.5f),
            new Vector2(1.5f, -0.5f),
            new Vector2(-0.5f, -2.5f),
            new Vector2(1.5f, -2.5f),

        };

        for (int i = 0; i < emptyPoints.Length; i++)
        {
            if (emptyPoints[i].x == x && emptyPoints[i].y == y)
            {
                return true;
            }
         }

        return false;
    }

    void ShowEndMessage()
    {

        string message;
        if (playersInGame > 0)
        {
            GameObject[] players = GameObject.FindGameObjectsWithTag("Player");

            GameObject alivePlayer = players.Where(player => !player.GetComponent<PlayerController>().isDead()).First();
            
            if (alivePlayer == null)
            {
                message = "LOL";
            } else
            {
                PhotonView playerView = alivePlayer.GetComponent<PhotonView>();

                string winnerName = "{NICKNAME}";

                for (int i = 0; i < PhotonNetwork.PlayerList.Length; i++)
                {
                    if (PhotonNetwork.PlayerList[i].ActorNumber == playerView.OwnerActorNr)
                    {
                        winnerName = PhotonNetwork.PlayerList[i].NickName;
                        break;
                    }
                }

                message = winnerName + " VENCEU!";
            }
            
            
        } else
        {
            message = "DRAW GAME!";
        }

        stageMessages.ShowMessage(message);
    }
}
