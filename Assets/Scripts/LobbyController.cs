﻿using Photon.Pun;
using Photon.Realtime;
using TMPro;
using UnityEngine;

public class LobbyController : MonoBehaviourPunCallbacks
{
    public GameObject enterRoomBtn;
 
    public GameObject startBtn;

    public GameObject cancelBtn;

    public int RoomSize = 5;

    public GameObject enterPanel;

    public GameObject inputName;

    public GameObject inputRoom;

    public GameObject roomPanel;

    public GameObject settingsPanel;

    public GameObject helpText;

    public int multiplayerSceneIndex;

    public GameObject roomNameText;

    public Transform playersContainer;

    public GameObject playerListPrefab;

    private TextMeshProUGUI _helpTextCmp;

    private void Start()
    {
        Cursor.visible = true;
        _helpTextCmp = helpText.GetComponent<TextMeshProUGUI>();
        roomPanel.SetActive(false);
        enterPanel.SetActive(true);
        enterRoomBtn.SetActive(false);
        settingsPanel.SetActive(false);
        _helpTextCmp.text = "Conectando...";

        if (PhotonNetwork.CurrentRoom != null)
        {
            if (PhotonNetwork.IsMasterClient)
            {
                PhotonNetwork.CurrentRoom.IsOpen = true;
            }
            OnJoinedRoom();
        } else
        {
            if (PlayerPrefs.HasKey("NickName"))
            {
                inputName.GetComponent<TMP_InputField>().text = PlayerPrefs.GetString("NickName");
            }
        }
    }


    public override void OnConnectedToMaster()
    {
        PhotonNetwork.AutomaticallySyncScene = true;
        enterRoomBtn.SetActive(true);
        _helpTextCmp.text = "Conectado ao servidor: " + PhotonNetwork.CloudRegion.ToUpper();
    }

    public void StartGame()
    {
        startBtn.SetActive(false);
        if (PhotonNetwork.IsMasterClient)
        {
            PhotonNetwork.CurrentRoom.IsOpen = false;
            Debug.Log("Starting Game");
            PhotonNetwork.LoadLevel(multiplayerSceneIndex);
            _helpTextCmp.text = "Starting...";
        }
    }

    public void ExitRoom()
    {
        if (PhotonNetwork.IsMasterClient)
        {
            for (int i = 0; i < PhotonNetwork.PlayerListOthers.Length; i++)
            {
                PhotonNetwork.CloseConnection(PhotonNetwork.PlayerListOthers[i]);
            }
        }
        PhotonNetwork.LeaveRoom();
    }

    public void EnterRoom()
    {
        string roomName = inputRoom.GetComponent<TMP_InputField>().text;

        if (string.IsNullOrEmpty(roomName))
        {
            _helpTextCmp.text = "Informe o nome da sala para entrar!";
            return;
        }

        string nickName = inputName.GetComponent<TMP_InputField>().text;

        if (string.IsNullOrEmpty(roomName))
        {
            nickName = "Player " + Random.Range(1, 1000);
        }

        PhotonNetwork.NickName = nickName;

        PlayerPrefs.SetString("NickName", nickName);

        Debug.Log("Room Name: " + roomName);

        roomNameText.GetComponent<TextMeshProUGUI>().text = "Sala: " + roomName;

        enterRoomBtn.SetActive(false);

        RoomOptions roomOpts = new RoomOptions()
        {
            IsVisible = true,
            IsOpen = true,
            MaxPlayers = (byte)RoomSize,
            CleanupCacheOnLeave = true
        };
        _helpTextCmp.text = "Criando sala";
        PhotonNetwork.CreateRoom(roomName, roomOpts);

    }

    public override void OnLeftRoom()
    {
        base.OnLeftRoom();
        roomPanel.SetActive(false);
        enterPanel.SetActive(true);
        enterRoomBtn.SetActive(true);
    }

    public override void OnJoinedRoom()
    {
        Debug.Log("Joined Room");
        enterPanel.SetActive(false);
        roomPanel.SetActive(true);
        startBtn.SetActive(PhotonNetwork.IsMasterClient);
        _helpTextCmp.text = "";

        ClearPlayerListings();
        ListPlayers();
    }

    public override void OnPlayerEnteredRoom(Player newPlayer)
    {
        base.OnPlayerEnteredRoom(newPlayer);
        ClearPlayerListings();
        ListPlayers();
    }

    public override void OnPlayerLeftRoom(Player otherPlayer)
    {
        base.OnPlayerLeftRoom(otherPlayer);
        ClearPlayerListings();
        ListPlayers();
    }

    public override void OnCreateRoomFailed(short returnCode, string message)
    {
        _helpTextCmp.text = message;
        string roomName = inputRoom.GetComponent<TMP_InputField>().text;
        PhotonNetwork.JoinRoom(roomName);
    }

    public override void OnJoinRoomFailed(short returnCode, string message)
    {
        _helpTextCmp.text = message;
        enterRoomBtn.SetActive(true);
    }

    void ClearPlayerListings()
    {
        for (int i = playersContainer.childCount - 1; i >= 0; i--)
        {
            playersContainer.GetChild(i).gameObject.SetActive(false);
        }
    }

    void ListPlayers()
    {
        foreach (Player player in PhotonNetwork.PlayerList)
        {
            GameObject tempListing = playersContainer.GetChild(player.ActorNumber - 1).gameObject;
            tempListing.GetComponent<TextMeshProUGUI>().text = player.NickName;
            tempListing.SetActive(true);
        }
    }

    public void ShowSettingsPanel()
    {
        settingsPanel.SetActive(true);
    }
}
