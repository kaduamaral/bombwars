﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TeleportBody : MonoBehaviour
{
    public Vector2 teleportArea;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Bomb"))
        {
            Debug.Log("Object Triggered: " + collision.gameObject.name);
            BombScript bombScript = collision.gameObject.GetComponent<BombScript>();
            bombScript.TeleportTo(teleportArea);
        }

        if (collision.gameObject.CompareTag("Item"))
        {
            Debug.Log("Object Triggered: " + collision.gameObject.name);
            ItemScript bombScript = collision.gameObject.GetComponent<ItemScript>();
            bombScript.TeleportTo(teleportArea);
        }
    }
}
