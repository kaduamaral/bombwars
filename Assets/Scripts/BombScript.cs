﻿using MilkShake;
using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Tilemaps;

public class BombScript : MonoBehaviour, IPunInstantiateMagicCallback
{

    float delay = 2.2f;
    float timeUntilExplode = 0;
    float timeLeftToExplode = 0;
    int force = 0;

    GameObject holder = null;
    public bool itsFlying = false;
    float flySpeed = 0.32f;
    Vector2 originalThrownPosition = Vector2.zero;
    Vector2 landingPoint = Vector2.zero;
    Vector2 throwDirection = Vector2.zero;

    public Rigidbody2D body;
    public BoxCollider2D boxCollider;
    public CircleCollider2D circleCollider;
    public AudioSource audioSource;
    public PhotonView photonView;
    SpriteRenderer render;

    GameObject owner;
    Tilemap groundMap;
    Shaker cameraShake;

    public ShakePreset ShakePreset;

    private Vector2 pushingDirection = Vector2.zero;

    bool teleport = false;
    Vector2 teleportArea;


    public GameObject explosionTop, 
                      explosionBottom, 
                      explosionMiddle,
                      explosionHorizontal,
                      explosionVertical,
                      explosionLeft,
                      explosionRight,
                      explosionSound;

    public void OnPhotonInstantiate(PhotonMessageInfo info)
    {
        Debug.Log("OnPhotonInstantiate " + info.photonView.InstantiationData.ToString());
        object[] instantiationData = info.photonView.InstantiationData;
        force = (int)instantiationData[0];
        delay = (float)instantiationData[1];
    }

    // Start is called before the first frame update
    void Start()
    {
        timeUntilExplode = Time.time + delay;
        groundMap = GameObject.Find("Ground").GetComponent<Tilemap>();
        cameraShake = GameObject.Find("Main Camera").GetComponent<Shaker>();
        render = GetComponent<SpriteRenderer>();

        // body = gameObject.GetComponent<Rigidbody2D>();
        // boxCollider = GetComponent<BoxCollider2D>();
        // circleCollider = GetComponent<CircleCollider2D>();
        // photonView = GetComponent<PhotonView>();
        // audioSource = GetComponent<AudioSource>();

        Collider2D[] colliders = Physics2D.OverlapCircleAll(transform.position, 0.5f);
        foreach (Collider2D collider in colliders)
        {
            if (collider.gameObject.CompareTag("Player"))
            {
                Physics2D.IgnoreCollision(boxCollider, collider, true);
            }
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (!itsFlying)
        {
            timeLeftToExplode = timeUntilExplode - Time.time;
            if (timeLeftToExplode <= 0)
            {
                Explode();
            }
        }
    }

    private void FixedUpdate()
    {
        if (body.bodyType == RigidbodyType2D.Dynamic)
        {
            Vector2 movement = Vector2.zero;

            if (holder != null)
            {
                movement = (Vector2)holder.transform.position + new Vector2(0, 0.7f);
            }
            else if (teleport)
            {
                movement = teleportArea;
                teleport = false;
            }
            else if (itsFlying)
            {
                float distance = Vector2.Distance(transform.position, landingPoint);
                if (distance <= 0.01)
                {
                    Debug.Log(string.Format("GROUNDING WITH: landing {0}; position {1}; distance {2}", landingPoint.ToString(), transform.position.ToString(), distance.ToString()));
                    movement = Ground();
                }
                else
                {
                    movement = Vector2.Lerp(transform.position, landingPoint, flySpeed);
                }
                
            }
            else if (!pushingDirection.Equals(Vector2.zero))
            {
                movement = body.position + pushingDirection * 6.5f * Time.fixedDeltaTime;
            }

            if (!movement.Equals(Vector2.zero))
            {
                body.MovePosition(movement);
            }
            
        }
    }

    public void TeleportTo(Vector2 _teleportArea)
    {
        if (teleport)
        {
            return;
        }

        teleport = true;
        teleportArea = transform.position;

        if (_teleportArea.x != 0)
        {
            teleportArea.x = _teleportArea.x;

            float travelled = transform.position.x - originalThrownPosition.x;

            landingPoint.x = _teleportArea.x + (landingPoint.x - (originalThrownPosition.x + travelled));
        }
        else if (_teleportArea.y != 0)
        {
            teleportArea.y = _teleportArea.y;

            float travelled = transform.position.y - originalThrownPosition.y;

            // landingPoint.y = landingPoint.y - originalThrownPosition.y + _teleportArea.y - 1;
            landingPoint.y = _teleportArea.y + (landingPoint.y - (originalThrownPosition.y + travelled));
        }

        landingPoint = GetCellCenter(landingPoint);

    }

    public Vector2 GetCellCenter(Vector2 position)
    {
        Vector3Int cellPosition = groundMap.WorldToCell(position);
        return groundMap.GetCellCenterWorld(cellPosition);
    }

    void Explode()
    {
        boxCollider.enabled = false;
        circleCollider.enabled = false;
        if (photonView.IsMine && owner)
        {
            owner.GetComponent<PlayerController>().BombExploded();
        }

        cameraShake.Shake(ShakePreset);

        List<GameObject> gameObjects = new List<GameObject>();
        
        gameObjects.Add(Instantiate(explosionMiddle, GetTilePosition(), Quaternion.identity));
        GameObject explosionSoundObj = Instantiate(explosionSound, GetTilePosition(), Quaternion.identity);

        bool hasTopWall = false;
        bool hasBottomWall = false;
        bool hasLeftWall = false;
        bool hasRightWall = false;

        for (int i = 1; i <= force; i++)
        {
            
            if (!hasTopWall) {
                Vector3 position = GetTilePosition(0, i);
                GameObject wall = GetWallOrItemAtPosition(position);

                if (wall != null) {
                    hasTopWall = true;
                    BurnSomething(wall);
                } else {
                    gameObjects.Add(Instantiate(explosionVertical, position, Quaternion.identity));
                }
            }

            if (!hasBottomWall)
            {
                Vector3 position = GetTilePosition(0, -1 * i);
                GameObject wall = GetWallOrItemAtPosition(position);

                if (wall != null)
                {
                    hasBottomWall = true;

                    BurnSomething(wall);
                }
                else
                {
                    gameObjects.Add(Instantiate(explosionVertical, position, Quaternion.identity));
                }
            }

            if (!hasLeftWall)
            {
                Vector3 position = GetTilePosition(-1 * i);
                GameObject wall = GetWallOrItemAtPosition(position);

                if (wall != null)
                {
                    hasLeftWall = true;

                    BurnSomething(wall);
                }
                else
                {
                    gameObjects.Add(Instantiate(explosionHorizontal, position, Quaternion.identity));
                }
            }

            if (!hasRightWall)
            {
                Vector3 position = GetTilePosition(i);
                GameObject wall = GetWallOrItemAtPosition(position);

                if (wall != null)
                {
                    hasRightWall = true;

                    BurnSomething(wall);
                }
                else
                {
                    gameObjects.Add(Instantiate(explosionHorizontal, position, Quaternion.identity));
                }
            }
        }

        if (!hasRightWall)
        {
            Vector3 position = GetTilePosition(force + 1);
            GameObject wall = GetWallOrItemAtPosition(position);
            if (wall != null)
            {
                BurnSomething(wall);
            } else
            {
                gameObjects.Add(Instantiate(explosionRight, position, Quaternion.identity));
            }
        }

        if (!hasLeftWall)
        {
            Vector3 position = GetTilePosition(-1 * (force + 1));
            GameObject wall = GetWallOrItemAtPosition(position);
            if (wall != null)
            {
                BurnSomething(wall);
            }
            else
            {
                gameObjects.Add(Instantiate(explosionLeft, position, Quaternion.identity));
            }
        }

        if (!hasTopWall)
        {
            Vector3 position = GetTilePosition(0, force + 1);
            GameObject wall = GetWallOrItemAtPosition(position);
            if (wall != null)
            {
                BurnSomething(wall);
            }
            else
            {
                gameObjects.Add(Instantiate(explosionTop, position, Quaternion.identity));
            }
        }

        if (!hasBottomWall)
        {
            Vector3 position = GetTilePosition(0, -1 * (force + 1));
            GameObject wall = GetWallOrItemAtPosition(position);
            if (wall != null)
            {
                BurnSomething(wall);
            }
            else
            {
                gameObjects.Add(Instantiate(explosionBottom, position, Quaternion.identity));
            }
        }

        // foreach (GameObject obj in gameObjects)
        // {
        //    Destroy(obj, 0.75f);
        //}

        Destroy(explosionSoundObj, 2.5f);

        Destroy(gameObject);

    }

    GameObject GetWallOrItemAtPosition(Vector3 pos)
    {
        Collider2D[] colliders = Physics2D.OverlapBoxAll(new Vector2(pos.x, pos.y), new Vector2(0.25f, 0.25f), 0);

        

        foreach (Collider2D collider in colliders)
        {
            if (collider.gameObject.CompareTag("Wall") || collider.gameObject.CompareTag("Brick") || collider.gameObject.CompareTag("Item"))
            {
                return collider.gameObject;
            }
        }
        

        return null;
    }


    void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Fire") && !itsFlying)
        {
            boxCollider.enabled = false;
            timeUntilExplode = Time.time + 0.05f;
        }
    }

    void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            Physics2D.IgnoreCollision(boxCollider, collision.gameObject.GetComponent<CircleCollider2D>(), false);
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (
            collision.gameObject.CompareTag("Wall") ||
            collision.gameObject.CompareTag("Brick") ||
            collision.gameObject.CompareTag("Bomb") 
            
        )
        {
            pushingDirection = Vector2.zero;
            body.bodyType = RigidbodyType2D.Static;
        }
    }

    Vector3 GetTilePosition(float xOffset = 0, float yOffset = 0)
    {
        Vector3 offset = new Vector3(xOffset, yOffset, 0);
        Vector3Int cellPosition = groundMap.WorldToCell(transform.position + offset);
        Vector3 position = groundMap.GetCellCenterWorld(cellPosition);

        return position;

    }

    public void Push(Vector2 direction)
    {
        body.bodyType = RigidbodyType2D.Dynamic;
        pushingDirection = direction;
    }

    public void Caught(GameObject holderObject)
    {

        body.bodyType = RigidbodyType2D.Dynamic;
        boxCollider.enabled = false;
        holder = holderObject;
        itsFlying = true;
        render.sortingLayerName = "Air";
    }

    public void Thrown(Vector2 fromPoint, Vector2 direction, int maxDistance)
    {
        Debug.Log("Throw: " + photonView.IsMine.ToString());

        throwDirection = direction;

        originalThrownPosition = fromPoint;

        landingPoint = CalculateLandingPoint(maxDistance);

        timeUntilExplode = Time.time + timeLeftToExplode;

        body.bodyType = RigidbodyType2D.Dynamic;
        boxCollider.enabled = false;
        holder = null;
        itsFlying = true;
        render.sortingLayerName = "Air";
    }

    Vector2 CalculateLandingPoint(int maxDistance)
    {
        for (int i = 2; i <= maxDistance; i++)
        {
            Vector2 point = GetCellCenter(originalThrownPosition + (throwDirection * i));
            Collider2D[] colliders = Physics2D.OverlapCircleAll(point, 0.35f);
            foreach (Collider2D collider in colliders)
            {
                if (collider.gameObject.CompareTag("Player"))
                {
                    return point;
                }
            }
        }

        return GetCellCenter(originalThrownPosition + (throwDirection * maxDistance));
    }

    Vector2 Ground()
    {
        Collider2D[] colliders = Physics2D.OverlapCircleAll(landingPoint, 0.35f);
        foreach (Collider2D collider in colliders)
        {
            if (collider.gameObject.GetInstanceID() == gameObject.GetInstanceID()) continue;

            if (
                collider.gameObject.CompareTag("Brick") || 
                collider.gameObject.CompareTag("Bomb") ||
                collider.gameObject.CompareTag("Item") ||
                collider.gameObject.CompareTag("Wall") ||
                collider.gameObject.CompareTag("Player")
            )
            {
                if (collider.gameObject.CompareTag("Bomb"))
                {
                    BombScript bombScript = collider.gameObject.GetComponent<BombScript>();
                    if (bombScript.itsFlying)
                    {
                        continue;
                    }
                }

                if (collider.gameObject.CompareTag("Item"))
                {
                    ItemScript bombScript = collider.gameObject.GetComponent<ItemScript>();
                    if (bombScript.isFlying)
                    {
                        continue;
                    }
                }

                if (collider.gameObject.CompareTag("Player"))
                {
                    PlayerController playerScript = collider.gameObject.GetComponent<PlayerController>();

                    if (playerScript.IsHolding("Bomb"))
                    {
                        Vector2 tmp = originalThrownPosition;
                        originalThrownPosition = landingPoint;
                        landingPoint = tmp;

                        return Vector2.Lerp(transform.position, landingPoint, flySpeed);
                    } else
                    {
                        playerScript.Stun();
                    }
                } else
                {
                    audioSource.PlayOneShot(audioSource.clip);
                }

                Debug.Log("Cannot ground because: " + collider.gameObject.name);
                
                landingPoint = landingPoint + throwDirection;
                
                return Vector2.Lerp(transform.position, landingPoint, flySpeed);
                
            }
        }

        Debug.Log("Grounded...");


        body.MovePosition(landingPoint);
        body.bodyType = RigidbodyType2D.Static;
        itsFlying = false;
        landingPoint = Vector2.zero;
        boxCollider.enabled = true;
        render.sortingLayerName = "Items";
        return Vector2.zero;
    }

    void BurnSomething(GameObject obj)
    {
        if (obj.CompareTag("Brick"))
        {
            BrickScript brickScript = obj.GetComponent<BrickScript>();
            brickScript.Burn();
        }

        if (obj.CompareTag("Item"))
        {
            obj.GetComponent<ItemScript>().Burn();
        }
    }

    public void SetOwner(GameObject _owner)
    {
        owner = _owner;
    }

}
