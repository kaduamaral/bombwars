﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.UI;

public class GameSettings : MonoBehaviour
{
    public AudioMixer audioMixer;

    Resolution[] resolutions;

    public TMP_Dropdown resolutionDropdown;
    public Toggle fullscreenToggle;
    public TMP_Dropdown qualityDropdown;
    public Slider volumeSlider;

    public GameObject settingsPanel;

    private void Start()
    {
        resolutions = Screen.resolutions;

        resolutionDropdown.ClearOptions();

        List<string> resolutionList = new List<string>();

        int currentResolutionIndex = 0;

        for (int i = 0; i < resolutions.Length; i++)
        {
            resolutionList.Add(resolutions[i].width + " x " + resolutions[i].height);

            if (resolutions[i].width == Screen.currentResolution.width && resolutions[i].height == Screen.currentResolution.height)
            {
                currentResolutionIndex = i;
            }
            
        }

        resolutionDropdown.AddOptions(resolutionList);
        resolutionDropdown.value = currentResolutionIndex;
        resolutionDropdown.RefreshShownValue();

        LoadPrefs();


    }

    void LoadPrefs()
    {
        Debug.Log("Loading Preferences");

        if (PlayerPrefs.HasKey("volume"))
        {
            float volume = GetVolume();
            SetVolume(volume);
            volumeSlider.value = volume;
        }

        return;

        if (PlayerPrefs.HasKey("quality"))
        {
            int quality = GetQuality();
            SetQuality(quality);
            qualityDropdown.value = quality;
        }

        if (PlayerPrefs.HasKey("fullScreen"))
        {
            bool fullScreen = GetFullscreen();
            SetFullscreen(fullScreen);
            fullscreenToggle.isOn = fullScreen;
        }

        if (PlayerPrefs.HasKey("resolutionWidth") && PlayerPrefs.HasKey("resolutionHeight"))
        {
            int resolution = GetResolution();
            SetResolution(resolution);
            resolutionDropdown.value = resolution;
        }



    }

    public void SetVolume(float volume)
    {
        audioMixer.SetFloat("volume", volume);
        PlayerPrefs.SetFloat("volume", volume);
    }

    public float GetVolume()
    {
        return PlayerPrefs.GetFloat("volume");
    }

    public void SetQuality(int qualityIndex)
    {
        QualitySettings.SetQualityLevel(qualityIndex);
        PlayerPrefs.SetInt("quality", qualityIndex);
    }

    public int GetQuality()
    {
        return PlayerPrefs.GetInt("quality");
    }

    public void SetFullscreen(bool isFullscreen)
    {
        Screen.fullScreen = isFullscreen;
        PlayerPrefs.SetInt("fullScreen", isFullscreen ? 1 : 0);
    }

    public bool GetFullscreen()
    {
        return PlayerPrefs.GetInt("fullScreen") == 1;
    }

    public void SetResolution(int resolutionIndex)
    {
        Resolution resolution = resolutions[resolutionIndex];
        Screen.SetResolution(resolution.width, resolution.height, Screen.fullScreen);
        PlayerPrefs.SetInt("resolutionWidth", resolution.width);
        PlayerPrefs.SetInt("resolutionHeight", resolution.height);
    }

    public int GetResolution()
    {
        int resolutionHeight = PlayerPrefs.GetInt("resolutionWidth");
        int resolutionWidth = PlayerPrefs.GetInt("resolutionHeight");
        for (int i = 0; i < resolutions.Length; i++)
        {
            if (resolutions[i].width == resolutionWidth && resolutions[i].height == resolutionHeight)
            {
                return i;
            }
        }

        return 0;
    }

    public void CloseSettingsPanel()
    {
        settingsPanel.SetActive(false);
    }
}