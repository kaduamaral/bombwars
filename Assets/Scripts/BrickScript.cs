﻿using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class BrickScript : MonoBehaviour
{
    Animator animator;
    GameObject stageManager;
    PhotonView photonView;

    float timeToDestroy;
    bool burned = false;

    public GameObject brickParticles;

    private void Start()
    {
        animator = GetComponent<Animator>();
        stageManager = GameObject.Find("StageManager");
        photonView = GetComponent<PhotonView>();
    }

    private void Update()
    {
        if (burned && Time.time >= timeToDestroy)
        {
            PutItem();
            DestroyBrick();
        }
    }


    public void Burn()
    {
        animator.SetTrigger("Fire");

        timeToDestroy = Time.time + 0.583f;
        burned = true;
        GameObject stonesObj = Instantiate(brickParticles, transform.position, Quaternion.identity);
        Destroy(stonesObj, 6);

    }

    public void PutItem()
    {
        if (!PhotonNetwork.IsMasterClient) return;

        StageScript stageScript = stageManager.GetComponent<StageScript>();

        List<string> itemsAvailable = new List<string>();
        int itemsCountAvailable = 0;
        foreach (string key in stageScript.itemsAvailable.Keys)
        {
            int itemCount = stageScript.itemsAvailable[key];
            if (itemCount > 0)
            {
                itemsCountAvailable += itemCount;
                itemsAvailable.Add(key);
            }
        }

        

        if (itemsCountAvailable == 0) return;

        GameObject[] bricks = GameObject.FindGameObjectsWithTag("Brick");

        int chance = Random.Range(1, bricks.Length);

        // Debug.Log(string.Format("Bricks {0}; Items Available {1}; Chance {2}", bricks.Length.ToString(), itemsCountAvailable.ToString(), chance.ToString()));

        if (itemsCountAvailable >= chance)
        {
            int itemI = Random.Range(0, itemsAvailable.Count - 1);
            
            string itemType = itemsAvailable[itemI];

            Debug.Log("Item getted " + itemI.ToString() + " - " + itemType);
            stageScript.CreatedItem(itemType);
            PhotonNetwork.Instantiate(Path.Combine("Prefabs", "Item" + itemType), transform.position, Quaternion.identity);

            return;
        }
    }

    void DestroyBrick()
    {
        if (PhotonNetwork.IsMasterClient)
        {
            PhotonNetwork.Destroy(gameObject);
        }
    }
}
