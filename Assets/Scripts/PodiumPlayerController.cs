﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PodiumPlayerController : MonoBehaviour
{
    public bool winner;
    // Start is called before the first frame update
    void Start()
    {
        Animator animator = GetComponent<Animator>();
        animator.SetTrigger(winner ? "Sorriu" : "Chorou");
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
