﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BurntGrassScript : MonoBehaviour
{
    Material material;
    
    float fade = 1f;
    float fadeSpeed = 0.5f;

    
    
    // Start is called before the first frame update
    void Start()
    {
        material = GetComponent<SpriteRenderer>().material;
    }

    // Update is called once per frame
    void Update()
    {
        fade -= fadeSpeed * Time.deltaTime;

        if (fade <= 0)
        {
            Destroy(gameObject);
        } else
        {
            material.SetFloat("_Fade", fade);
        }


    }
}
