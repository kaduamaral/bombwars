﻿using Photon.Pun;
using System;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.Tilemaps;

public class PlayerController : MonoBehaviour
{

    private float speed = 3.6f;

    private float patinsSpeed = 0.7f;

    private float stunnedTime = 0;

    public static Vector2 FOOT_OFFSET = new Vector2(0, -0.4f);

    private Rigidbody2D body;
    private CircleCollider2D circleCollider2D;
    private AudioSource deathSound;
    private PhotonView photonView;

    private Animator anim;

    Vector2 movement;

    public GameObject defaultBomb;

    int bombs = 1;
    int bombForce = 1;
    int usedBombs = 0;
    bool hasPezinho = false;
    bool hasLuvinha = false;
    bool hasSoquinho = false;
    bool morreu = false;
    float destroyTime = 0;
    private int patins = 0;

    Curse curse = Curse.None;

    private GameObject objectCaught;

    Tilemap groundMap;
    StageScript stage;

    // Start is called before the first frame update
    void Start()
    {
        photonView = gameObject.GetComponent<PhotonView>();
        body = gameObject.GetComponent<Rigidbody2D>();
        anim = gameObject.GetComponent<Animator>();
        circleCollider2D = gameObject.GetComponent<CircleCollider2D>();
        deathSound = gameObject.GetComponent<AudioSource>();
        groundMap = GameObject.Find("Ground").GetComponent<Tilemap>();
        anim.SetFloat("StopVertical", -1);
        stage = GameObject.Find("StageManager").GetComponent<StageScript>();
    }

    // Update is called once per frame
    void Update()
    {

        if (IsMine() && morreu && Time.time >= destroyTime)
        {
            // PhotonNetwork.DestroyPlayerObjects(PhotonNetwork.LocalPlayer);
            PhotonNetwork.Destroy(gameObject);
        }

        if (!IsMine() || morreu || stage.isGameOver() || !stage.isGameStarted())
        {
            return;
        }

        if (stunnedTime > 0)
        {
            stunnedTime -= Time.deltaTime;

            Debug.Log("Stun time left: " + stunnedTime.ToString());

            if (stunnedTime <= 0)
            {
                Unstun();
            }
            else
            {
                return;
            }
        }

        movement.x = Input.GetAxisRaw("Horizontal");
        movement.y = Input.GetAxisRaw("Vertical");

        if (curse == Curse.InverseMovement)
        {
            movement = movement * -1;
        }

        if (Input.GetButtonDown("Jump"))
        {
            FireTrigger(GetCellCenter(GetFootPosition()));
        }

        if (curse == Curse.PutAllBombs && objectCaught != null)
        {
            PutBomb(transform.position);
        }

        if (Input.GetButtonUp("Jump") && objectCaught != null)
        {
            ThrownObject();
        }

        if (hasSoquinho && Input.GetKeyDown(KeyCode.Q))
        {
            Punch();
        }

        if (curse == Curse.DropItems)
        {
            ThrowItemsByCurse();
        }

    }

    void FixedUpdate()
    {
        if (!IsMine() || morreu || stage.isGameOver() || !stage.isGameStarted())
        {
            return;
        }

        PlayerMove();
    }

    void PlayerMove()
    {
        float patinsEffect = patins * patinsSpeed;

        if (curse == Curse.HighSpeed)
        {
            patinsEffect = 5 * patinsSpeed;
        } else if (curse == Curse.LowSpeed)
        {
            patinsEffect = -1.5f;
        }

        float speedModify = speed + patinsEffect;
        Vector2 movementCalculated = body.position + movement * speedModify * Time.fixedDeltaTime;
        body.MovePosition(movementCalculated);

        UpdateAnimation();
    }

    void UpdateAnimation()
    {
        if (!IsMine()) return;

        anim.SetFloat("Horizontal", movement.x);
        anim.SetFloat("Vertical", movement.y);
        anim.SetFloat("Speed", movement.sqrMagnitude);

        if (movement.sqrMagnitude != 0)
        {
            anim.SetFloat("StopHorizontal", movement.x);
            anim.SetFloat("StopVertical", movement.y);
        }
    }

    void FireTrigger(Vector2 position)
    {

        Collider2D[] colliders = Physics2D.OverlapBoxAll(position, new Vector2(0.25f, 0.25f), 0);

        foreach (Collider2D collider in colliders)
        {

            if (collider.gameObject == gameObject) continue;

            if (hasLuvinha && (collider.gameObject.CompareTag("Player") || collider.gameObject.CompareTag("Bomb")))
            {

                CatchObject(collider.gameObject);
                return;
            }
            if (collider.gameObject.CompareTag("Bomb"))
            {
                return;
            }
        }

        PutBomb(position);
    }

    void PutBomb(Vector2 position)
    {
        if (usedBombs >= bombs || (curse == Curse.OneBomb && usedBombs >= 1))
        {
            return;
        }

        usedBombs++;

        int _bombForce = bombForce;
        float _timeToExplode = 2.2f;

        switch (curse)
        {
            case Curse.WeakFire:
                _bombForce = 0;
                break;
            case Curse.StrongFire:
                _bombForce = 7;
                break;
            case Curse.SlowExplosion:
                _timeToExplode = 4.2f;
                break;
            case Curse.FastExplosion:
                _timeToExplode = 1f;
                break;
            default:
                break;
        }


        object[] customData = new object[2] {
            _bombForce,
            _timeToExplode
        };

        // GameObject bomb = Instantiate(defaultBomb, position, Quaternion.identity);
        GameObject bomb = PhotonNetwork.Instantiate(Path.Combine("Prefabs", "Bomb"), position, Quaternion.identity, 0, customData);
        BombScript bombScript = bomb.GetComponent<BombScript>();
        bombScript.SetOwner(gameObject);
    }

    void CatchObject(GameObject obj)
    {
        objectCaught = obj;
        anim.SetBool("Carregando", true);
        if (objectCaught.CompareTag("Bomb"))
        {
            objectCaught.GetComponent<BombScript>().Caught(gameObject);
        }

        if (IsMine())
        {
            photonView.RPC("RPCCatchObject", RpcTarget.Others, objectCaught.GetComponent<PhotonView>().ViewID);
        }
    }

    public bool IsHolding(string objectTag)
    {
        return objectCaught != null && objectCaught.CompareTag(objectTag);
    }

    [PunRPC]
    void RPCCatchObject(int objectCaughtViewID)
    {
        PhotonView objView = PhotonView.Find(objectCaughtViewID);

        CatchObject(objView.gameObject);
    }

    void ThrownObject()
    {
        Vector2 direction = new Vector2(anim.GetFloat("StopHorizontal"), anim.GetFloat("StopVertical"));
        Vector2 originalPosition = GetFootPosition();
        if (objectCaught.CompareTag("Bomb"))
        {

            objectCaught.GetComponent<BombScript>().Thrown(originalPosition, direction, 5);

        }

        photonView.RPC("RPCThrownObject", RpcTarget.Others, objectCaught.GetPhotonView().ViewID, direction.x, direction.y, originalPosition.x, originalPosition.y);


        anim.SetBool("Carregando", false);
        objectCaught = null;
    }

    [PunRPC]
    void RPCThrownObject(int objectCaughtViewID, float directionX, float directionY, float originalPosX, float originalPosY)
    {
        Vector2 direction = new Vector2(directionX, directionY);
        Vector2 originalPosition = new Vector2(originalPosX, originalPosY);

        if (!objectCaught || objectCaught.GetPhotonView().ViewID != objectCaughtViewID)
        {
            objectCaught = PhotonView.Find(objectCaughtViewID).gameObject;
        }

        if (objectCaught.CompareTag("Bomb"))
        {
            objectCaught.GetComponent<BombScript>().Thrown(originalPosition, direction, 5);
        }

        objectCaught = null;
    }


    public Vector2 GetFootPosition()
    {
        return (Vector2)transform.position + FOOT_OFFSET;
    }

    public Vector2 GetCellCenter(Vector2 position)
    {
        Vector3Int cellPosition = groundMap.WorldToCell(position);
        return groundMap.GetCellCenterWorld(cellPosition);
    }

    public void BombExploded()
    {
        usedBombs--;
    }



    void OnCollisionEnter2D(Collision2D collision)
    {

        if (collision.gameObject.CompareTag("Player"))
        {
            Physics2D.IgnoreCollision(this.circleCollider2D, collision.gameObject.GetComponent<Collider2D>(), true);
            return;
        }

        if (!IsMine()) return;

        if (hasPezinho && collision.gameObject.CompareTag("Bomb"))
        {
            Vector2 positionDifference = (Vector2)collision.gameObject.transform.position - GetFootPosition();

            Vector2 direction = new Vector2(
                Math.Abs(positionDifference.x) < 0.5 ? 0 : (float)Math.Round(positionDifference.x, 0),
                Math.Abs(positionDifference.y) < 0.5 ? 0 : (float)Math.Round(positionDifference.y, 0)
            );

            PushBomb(collision.gameObject, direction);
            photonView.RPC("RPCPushBomb", RpcTarget.Others, collision.gameObject.GetPhotonView().ViewID, direction.x, direction.y);
            // collision.gameObject.GetComponent<BombScript>().Push(direction);
        }
    }

    void PushBomb(GameObject bomb, Vector2 direction)
    {
        bomb.GetComponent<BombScript>().Push(direction);
    }

    [PunRPC]
    void RPCPushBomb(int viewID, float directionX, float directionY)
    {
        GameObject bomb = PhotonView.Find(viewID).gameObject;
        Vector2 direction = new Vector2(directionX, directionY);
        PushBomb(bomb, direction);
    }

    void OnTriggerEnter2D(Collider2D collision)
    {

        if (!IsMine()) return;

        if (collision.gameObject.CompareTag("Item"))
        {
            PickUpItem(collision.gameObject);

        }
        else if (collision.gameObject.CompareTag("Fire"))
        {
            Morreu();
        }
    }

    void PickUpItem(GameObject itemObject)
    {
        ItemScript itemScript = itemObject.GetComponent<ItemScript>();

        if (itemScript.isFlying) return;

        itemObject.GetComponent<BoxCollider2D>().enabled = false;

        Item itemName = itemScript.type;

        if (itemName != Item.Caveira)
        {
            BreakCurse();
        }

        AddItemEffect(itemName, itemScript.GetCurse());

        itemScript.DestroyItem();
    }

    public void Stun()
    {
        anim.SetTrigger("Tonto");

        if (!IsMine()) return;

        stunnedTime = 1;
        DropItems(curse == Curse.None ? 4 : -1);

        if (curse != Curse.None)
        {
            BreakCurse();
        }
    }

    void Unstun()
    {
        stunnedTime = 0;
    }

    public void Morreu()
    {
        morreu = true;
        circleCollider2D.enabled = false;
        anim.SetTrigger("Morreu");
        deathSound.Play();
        destroyTime = Time.time + 1;

        if (IsMine())
        {
            stage.PlayerDie();
        }
    }

    public bool isDead()
    {
        return morreu;
    }

    bool IsMine()
    {
        if (photonView == null) return false;

        return photonView.IsMine;
    }

    void SetCurse(Curse _curse)
    {
        curse = _curse;
        anim.SetBool("Cursed", true);
        if (curse == Curse.DropItems)
        {
            nextDropItemTime = Time.time + 1;
        }
    }

    void BreakCurse()
    {
        if (curse == Curse.None) return;
        ThrowItemAway(Item.Caveira, GetRandomDirection());
        curse = Curse.None;
        nextDropItemTime = 0;
        anim.SetBool("Cursed", false);
    }

    void ThrowItemAway(Item itemType, Vector2 throwDirection)
    {
        GameObject item = PhotonNetwork.Instantiate(Path.Combine("Prefabs", "Item" + itemType), GetCellCenter(GetFootPosition()), Quaternion.identity);
        ItemScript itemScript = item.GetComponent<ItemScript>();
        if (itemType == Item.Caveira)
        {
            itemScript.SetCurse(curse == Curse.DropItems ? curse : curse + 1);
        }
        itemScript.Fly(throwDirection);
    }

    float nextDropItemTime = 0;

    void ThrowItemsByCurse()
    {
        if (Time.time >= nextDropItemTime)
        {
            DropItems(1);
            nextDropItemTime = Time.time + 1;
        }
        
    }

    void DropItems(int itemsToDrop)
    {
        List<Item> itemsList = GetItemsList();

        if (curse != Curse.None || itemsList.Count < itemsToDrop || itemsToDrop == -1)
        {
            itemsToDrop = itemsList.Count;
        }

        Vector2 throwDirection;

        Vector2[] throwDirections = new Vector2[4] {
            new Vector2(1, 0),
            new Vector2(0, 1),
            new Vector2(-1, 0),
            new Vector2(0, -1),
        };

        int currentDirection = 0;

        for (int i = 0; i < itemsToDrop; i++)
        {
            if (itemsToDrop > 1)
            {
                throwDirection = throwDirections[currentDirection];
            } else
            {
                throwDirection = GetRandomDirection();
            }
            

            Item item = itemsList[i];
            RemoveItemEffect(item);
            ThrowItemAway(item, throwDirection);
            currentDirection++;

            if (currentDirection > 3) currentDirection = 0;
        }
    }

    void AddItemEffect(Item itemName, Curse _curse)
    {
        switch (itemName)
        {
            case Item.Patins:
                patins++;
                break;
            case Item.Bomba:
                bombs++;
                break;
            case Item.Fogo:
                bombForce++;
                break;
            case Item.Pezinho:
                hasPezinho = true;
                break;
            case Item.Luva:
                hasLuvinha = true;
                break;
            case Item.Soquinho:
                hasSoquinho = true;
                break;
            case Item.Caveira:
                SetCurse(_curse);
                break;
            default:
                break;
        }
    }

    void RemoveItemEffect(Item itemName)
    {
        switch (itemName)
        {
            case Item.Patins:
                patins--;
                break;
            case Item.Bomba:
                bombs--;
                break;
            case Item.Fogo:
                bombForce--;
                break;
            case Item.Pezinho:
                hasPezinho = false;
                break;
            case Item.Luva:
                hasLuvinha = false;
                break;
            case Item.Soquinho:
                hasSoquinho = false;
                break;
            case Item.Caveira:
                curse = Curse.None;
                break;
            default:
                break;
        }
    }


    void AddItemsToList(List<Item> list, Item item, int count)
    {
        for (int i = 0; i < count; i++)
        {
            list.Add(item);
        }
    }

    List<Item> GetItemsList()
    {
        List<Item> itemsList = new List<Item>();

        if (patins > 0)
        {
            AddItemsToList(itemsList, Item.Patins, patins);
        }

        if (bombs > 1)
        {
            AddItemsToList(itemsList, Item.Bomba, bombs - 1);
        }

        if (bombForce > 1)
        {
            AddItemsToList(itemsList, Item.Fogo, bombForce - 1);
        }

        if (hasPezinho)
        {
            itemsList.Add(Item.Pezinho);
        }

        if (hasLuvinha)
        {
            itemsList.Add(Item.Luva);
        }

        if (hasSoquinho)
        {
            itemsList.Add(Item.Soquinho);
        }

        return ShuffleList(itemsList);
    }

    List<Item> ShuffleList(List<Item> list)
    {
        System.Random rng = new System.Random();

        int n = list.Count;
        while (n > 1)
        {
            n--;
            int k = rng.Next(n + 1);
            Item value = list[k];
            list[k] = list[n];
            list[n] = value;
        }

        return list;
    }

    Vector2 GetRandomDirection()
    {
        int x = UnityEngine.Random.Range(-1, 1);
        int y = UnityEngine.Random.Range(-1, 1);

        if (Math.Abs(x) == Math.Abs(y))
        {
            return GetRandomDirection();
        }
        else
        {
            return new Vector2(x, y);
        }
    }

    void Punch()
    {
        Debug.Log("Punch");
        Vector2 direction = new Vector2(anim.GetFloat("StopHorizontal"), anim.GetFloat("StopVertical"));
        Vector2 footPosition = GetFootPosition();
        Collider2D[] colliders = Physics2D.OverlapCircleAll(footPosition + direction, 0.35f);

        foreach (Collider2D collider in colliders)
        {
            if (collider.gameObject.GetInstanceID() == gameObject.GetInstanceID()) continue;

            if (collider.gameObject.CompareTag("Bomb"))
            {
                Debug.Log("Obj to Punch: " + collider.gameObject.name);

                BombScript bombScript = collider.gameObject.GetComponent<BombScript>();

                bombScript.Thrown(collider.gameObject.transform.position, direction, 2);

                photonView.RPC("RPCPunch", RpcTarget.Others, collider.gameObject.GetPhotonView().ViewID, direction.x, direction.y, collider.gameObject.transform.position.x, collider.gameObject.transform.position.y);
                return;
            }
        }
    }

    [PunRPC]
    void RPCPunch(int objectPunchedtViewID, float directionX, float directionY, float originalPosX, float originalPosY)
    {
        Vector2 direction = new Vector2(directionX, directionY);
        Vector2 originalPosition = new Vector2(originalPosX, originalPosY);

        GameObject punchedObject = PhotonView.Find(objectPunchedtViewID).gameObject;
        

        if (punchedObject.CompareTag("Bomb"))
        {
            punchedObject.GetComponent<BombScript>().Thrown(originalPosition, direction, 2);
        }
    }

}