﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireScript : MonoBehaviour
{

    float timeToRemove = 0.75f;

    public GameObject burstGrass;

    // Update is called once per frame
    void Update()
    {
        timeToRemove -= Time.deltaTime;

        if (timeToRemove <= 0)
        {
            Destroy(gameObject);
            Instantiate(burstGrass, transform.position, Quaternion.identity);
        }
    }
}
